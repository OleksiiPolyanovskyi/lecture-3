export const getTextFromServer = async id => {
  const response = await fetch(`http://localhost:3002/game/texts/${id}`);
  return response.json();
};

