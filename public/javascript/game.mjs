
const username = sessionStorage.getItem("username");
let arrayOfChars=[];
let gameCounter = 0;

if (!username) {
  window.location.replace("/login");
}

const socket = io("", { query: { username } });

socket.on('ERROR', errorHandler);
socket.on('USERS_UPDATE',(users) => renderUsers(users, username));
socket.on('START_GAME', startGame);
socket.on('FINISH_GAME', finishGameHandler)

const startButton = document.getElementById('ready-btn');
const waitButton = document.getElementById('wait-btn');
const disconnectButton = document.getElementById('disconnect');

disconnectButton.onclick = () => disconnectHandler();

startButton.onclick = () => {
  socket.emit('USER_STATUS_READY');
  startButton.classList.add('display-none');
  disconnectButton.classList.add('display-none');
  waitButton.classList.remove('display-none');
};


function errorHandler(){
  disconnectHandler();
  alert("User with this name is already exists!");  
};

function disconnectHandler() {  
  sessionStorage.removeItem("username");
  window.location.replace("/login");
};

function renderUsers(users, username) {  
  console.log('render users');
  const usersContainer = document.getElementById('users');
  usersContainer.innerHTML = '';  
  users.forEach(user => {
    const name = username === user.name ? `${user.name} (you)` : user.name;
    usersContainer.insertAdjacentHTML('beforeend', createUserTemplate(user, name));
    const statusCircle = document.getElementById(`status-${user.name}`);
    const progressBar = document.getElementById(`progress_bar-${user.name}`);
    if (user.ready) {
      statusCircle.classList.add('ready');
    }
    progressBar.style.width = `${user.progress}%`;   
  })  
};

function createUserTemplate(user, name){
  if (!user) return
  return `
  <div class="user-header"><div id="status-${user.name}" class="circle"></div> <h4>${name}</h4></div> 
  <div class="progress_bar">
    <div id="progress_bar-${user.name}" class="progress_bar-inner"></div>
  </div>
  `;
};

async function startGame(timeBeforeStart, timeForGame, textIndex){  
  const res = await getTextFromServer(textIndex);
  const modified = modifyText(res.text);
  startTimers(timeBeforeStart, timeForGame, modified);
  activateEventListener(); 
};

async function getTextFromServer(id) { 
  const response = await fetch(`http://localhost:3002/game/texts/${id}`);
  return response.json();
};

function modifyText(text){
  arrayOfChars = text.split('');
  const modified = arrayOfChars.map((char, index) => {
   return char === ' '?
    `<span id="${char}${index}">&nbsp;</span>`:
    `<span id="${char}${index}">${char}</span>`
  }).join('');
  return modified;
}

function startTimers(timeBeforeStart,timeForGame, text){
  const timerCont = createElement({tagName:'div', className:'game-timer', attributes:{id:'game-timer'}});  
  const container = document.getElementById('game');
  let period = timeBeforeStart;  
  const timer = setInterval(()=>{
    container.innerHTML = period;
    period--;
   
    if(period < 0) {
      container.innerHTML = text;
      container.appendChild(timerCont);
      startGameTimer(timeForGame);
      clearInterval(timer);
    }
  },1000)
};

function activateEventListener(){
  window.addEventListener("keyup", onKeyUpHandler);
};

function onKeyUpHandler(event){  
  let curChar = arrayOfChars[gameCounter];  
  let charId = `${curChar}${gameCounter}`;
  let charEl = document.getElementById(charId); 
  let nextChar = arrayOfChars[gameCounter+1];
  let nextcharId = `${nextChar}${gameCounter+1}`;
  let nextcharEl = document.getElementById(nextcharId);
  if (event.key === curChar) {
    charEl.classList.add('colored');
    if (nextcharEl){
      nextcharEl.classList.add('underscored');
    }    
    gameCounter++;
    const progress = countProgress(gameCounter, arrayOfChars);
    socket.emit('UPDATE_PROGRESS', progress);
  }
}

function startGameTimer(timeForGame){  
  const timerCont = document.getElementById('game-timer');  
  let period = timeForGame;  
  const timer = setInterval(()=>{
    timerCont.innerHTML = period;
    period--;   
    if(period < 0) {      
      clearInterval(timer);
      socket.emit('FINISH_GAME');
    }
  },1000) 
}


function countProgress(gameCounter, arrayOfChars){
  return gameCounter*100/arrayOfChars.length;
};

function finishGameHandler(users) {
  const timerCont = document.getElementById('game-timer');
  timerCont.classList.add('display-none');
  startButton.classList.remove('display-none');
  disconnectButton.classList.remove('display-none');
  alert('Game over');
}

const createElement = ({ tagName, className, attributes = {} }) => {
  const element = document.createElement(tagName);
  if (className) {
    addClass(element, className);
  }
  Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));
  return element;
};

const addClass = (element, className) => {
  const classNames = formatClassNames(className);
  element.classList.add(...classNames);
};

const removeClass = (element, className) => {
  const classNames = formatClassNames(className);
  element.classList.remove(...classNames);
};

const formatClassNames = className => className.split(" ").filter(Boolean);