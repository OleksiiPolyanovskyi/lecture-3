import { users } from './users-data';

import { 
  allowedToJoin, 
  createUser, 
  removeUser, 
  updateUserStatus, 
  updateUserProgress,
  finishGameHandler 
} from './user-helpers';
  
export default io => {
  io.on('connection', socket => {
    const username = socket.handshake.query.username;
    if (allowedToJoin(users, username)) {
      const newUser = createUser(username);
      users.push(newUser);
      socket.emit('USERS_UPDATE', users);
      socket.broadcast.emit('USERS_UPDATE', users);
    }else {
      socket.emit('ERROR');
    } 

    socket.on('disconnect', removeUser(users, username, socket));
    socket.on('USER_STATUS_READY', updateUserStatus(users, username, socket));
    socket.on('UPDATE_PROGRESS', (progress) => updateUserProgress(progress, users, username, socket));
    socket.on('FINISH_GAME', () => finishGameHandler(socket, users));
  });
};

