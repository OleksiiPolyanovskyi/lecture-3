import { texts } from '../data';
import * as config from './config';

export function allowedToJoin(users, username) {
    return users.filter( user => user.name === username).length === 0;
  };
  
export function createUser(username) {
    return {
      name: username,
      ready: false,
      progress: 0
    };
  }; 
  
export function removeUser(users, username, socket){  
    return () => {    
      const index = users.findIndex( user => user.name === username);
      users.splice(index, 1);
      socket.broadcast.emit('USERS_UPDATE', users);
    };
  };
  
export function updateUserStatus(users, username, socket){  
    return () => {    
      const index = users.findIndex( user => user.name === username);
      const updated = users[index];
      updated.ready = true;
      users.splice(index, 1, updated);
      socket.emit('USERS_UPDATE', users);
      socket.broadcast.emit('USERS_UPDATE', users);
      startGame(users, socket);
    };
  };
  
export function updateUserProgress(progress ,users, username, socket){      
      const index = users.findIndex( user => user.name === username);
      const updated = users[index];
      updated.progress = progress;
      if (progress >= 100){
        socket.emit('FINISH_GAME', users);
        socket.broadcast.emit('FINISH_GAME', users); 
      }
      users.splice(index, 1, updated);
      socket.emit('USERS_UPDATE', users);
      socket.broadcast.emit('USERS_UPDATE', users);        
  }

export function finishGameHandler(socket, users){
    socket.emit('FINISH_GAME', users);
  };


function startGame(users, socket){
    const notReadyUsers = users.filter(user => user.ready === false);
    if (notReadyUsers.length === 0){
      const numberOfTexts = texts.length;
      const textIndex = getRandomInt(numberOfTexts);
      socket.emit(
        'START_GAME', 
        config.SECONDS_TIMER_BEFORE_START_GAME,
        config.SECONDS_FOR_GAME,
        textIndex
        );
      socket.broadcast.emit(
        'START_GAME', 
        config.SECONDS_TIMER_BEFORE_START_GAME,
        config.SECONDS_FOR_GAME,
        textIndex
        );
    }
  }
  
  function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }